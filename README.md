# Intro

thist project has four branch(without main) with no itend of mergeing. Evry branch represent different way of configuration
of DataSource and Jdbc Driver


Branches:
* main- part of code that dont depend from type of JDBC implementatio
* h2- Spring Boot make  data source and jdbc with auto config
* mysql-app_conf - Spring Boot make data source and jdbc via application.properties
* mysql-custom_conf - custom configuration via @Configuration
* jpa -  difference between this and previously three branch to who what JPA abstraction and it implementation do underhood, in Service we have same findAllBattle() but in this branch it is auto config via JPA


A section of text follows is notes  of "Spring in action"(chapter 12 and 14) and explains the branches of the project

# 1.DataSource, JDBC, JDBC driver, Jdbc template,Repository



The data source is a component that manages connections to the database management systems (DBMS). The data source uses the JDBC driver to get the connections it manages

Without an object taking the responsibility of a data source, the app would need to request a new connection for each operation with the data

* JDBC is abstraction for connection with different database
* JDBC Driver is implementation of that abstraction
* Different drivers for different database
* requesting a new connection and authenticating each operation again and again for each is a waste
* A data source object can efficiently manage the connections to minimize the number of unnecessary operations.

For Java apps, you have multiple choices for data source implementations, but the most commonly used today is the HikariCP

Using JDBC classes provided by the JDK has not proven to be a comfortable way to work with persisted data

JDBC template is simpler then JDBC Driver:

String sql = "INSERT INTO purchase VALUES (?,?)";
try (PreparedStatement stmt = con.prepareStatement(sql)) {
stmt.setString(1, name);
stmt.setDouble(2, price);
stmt.executeUpdate();
} catch (SQLException e) {
// do something when an exception occurs
}



We implement all JDBC Template the capabilities related to the persistence layer in classes Repository

examples don’t depend on the relational database technology you choose.
You can choose to implement the examples with some other relational database technology, like Postgres, Oracle, or MS SQL

To  get Repository instancewe can make it with annotation @Component and @Configuration, but Spring provides a annotation  @Repository.

# 1.1. Spring boot auto config JdbcTemplate and data surce for h2


Repository need JdbcTemplate bean, but we dont need to inject it. Spring Boot made instance of JdbcTemplate and data source when he saw h2( or mysql-connector-java) dependency


If you use Spring but not Spring Boot, you need to define the DataSource bean and JdbcTemplate

Restult of  JdbcTemplate querry is ResultSet we need to map into Entity object
We do that with ROW Mapper


# 2.Customizing the configuration of the data source(connect to MySQL)


## 2.1. Defining the data source in the application properties file

We need to:
1. Change the project dependencies to exclude H2 and add the adequate JDBC driver.

2. Add the connection properties for the new database to the “application.properties” file.


<dependency>    
   <groupId>mysql</groupId>    
   <artifactId>mysql-connector-java</artifactId>     
   <scope>runtime</scope>   
</dependency>    

#
We add the MySQL JDBC driver as a runtime dependency. Insted off

<dependency>                                             
   <groupId>com.h2database</groupId>                     
   <artifactId>h2</artifactId>                           
   <scope>runtime</scope>                                
</dependency>

#
application.properties

spring.datasource.url=jdbc:mysql://localhost/spring_quickly?
useLegacyDatetimeCode=false&serverTimezone=UTC          ❶

spring.datasource.username=<dbms username>              ❷
spring.datasource.password=<dbms password>              ❷
spring.datasource.initialization-mode=always            ❸


❶ We configure the URL that defines the location to the database.

❷ We configure the credentials to authenticate and get connections from the DBMS.

❸ We set the initialization mode to “always” to instruct Spring Boot to run the queries in the “schema.sql” file.


With these couple of changes, the application now uses the MySQL database. Spring Boot knows to create the DataSource bean using the spring.datasource properties you provided in the “application.properties”

## 2.2.Using a custom DataSource bean

Spring Boot knows how to use a DataSource bean if you provide the connection details in the “application.properties” file. Sometimes this is enough.In such a case, you need to define the bean yourself.

You need to define the bean yourself, create a configuration file and define a method annotated with @Bean, which returns the DataSource instance we add to the Spring context

Values for the properties in @Configuration are injected using the @Value annotation. In the “application.properties” file these properties  they are intentionally named  “custom” in their name to stress that we chose these names, and they’re not Spring Boot properties. You can give these properties any name.



