package com.example.datasourcinjava;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DataSourcInJavaApplication {

	public static void main(String[] args) {
		SpringApplication.run(DataSourcInJavaApplication.class, args);
	}

}
