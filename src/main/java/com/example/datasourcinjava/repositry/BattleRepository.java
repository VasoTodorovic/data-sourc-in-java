package com.example.datasourcinjava.repositry;


import com.example.datasourcinjava.entity.Battle;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.util.List;


//When Spring Boot saw you added the H2 dependency in pom.xml, it automatically configured a data source and a JdbcTemplate instance. In this example, we’ll use them directly.
//
//If you use Spring but not Spring Boot, you need to define the DataSource bean and the JdbcTemplate bean (you can add them in the Spring context using the @Bean annotation in the configuration class
@Repository
public class BattleRepository {
    private final JdbcTemplate jdbc;


//
    public BattleRepository(JdbcTemplate jdbc) {
        this.jdbc = jdbc;
    }

    public void insertBattle(Battle battle) {
        String sql =
        "INSERT INTO purchase VALUES (?, ?, ?,?)";

        jdbc.update(sql,
                battle.getName(),
                battle.getDate_battle(),
                battle.getParticipant());
    }


    public List<Battle> findAllBattle() {
        String sql = "SELECT * FROM battles";

        RowMapper<Battle> battleRowMapper = (r, i) -> {
            Battle rowObject = new Battle();
            rowObject.setId(r.getInt("id"));
            rowObject.setName(r.getString("name"));
            rowObject.setDate_battle(r.getString("date_battle"));
            rowObject.setParticipant(r.getInt("participant"));
            return rowObject;
        };

        return  jdbc.query(sql,battleRowMapper);
    }
}
