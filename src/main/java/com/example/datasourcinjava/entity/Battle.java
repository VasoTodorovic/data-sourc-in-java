package com.example.datasourcinjava.entity;

public class Battle {
    private int id;
    private String name;
    private String date_battle;
    private int participant;

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDate_battle(String date_battle) {
        this.date_battle = date_battle;
    }

    public void setParticipant(int participant) {
        this.participant = participant;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDate_battle() {
        return date_battle;
    }

    public int getParticipant() {
        return participant;
    }
}
