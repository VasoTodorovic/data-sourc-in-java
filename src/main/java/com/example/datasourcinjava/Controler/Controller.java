package com.example.datasourcinjava.Controler;


i
import com.example.datasourcinjava.entity.Battle;
import com.example.datasourcinjava.service.BattleService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

@RestController
@RequestMapping("/battles")
public class Controller {
    public final BattleService battleService;


    public Controller(BattleService battleService) {
        this.battleService = battleService;
    }

    public ResponseEntity<List<Battle>> getAll(){
        List<Battle> list= battleService.getAll();
        return ResponseEntity.status(HttpStatus.ACCEPTED)
                .header("all the battles")
                .body(list);
    }

}
