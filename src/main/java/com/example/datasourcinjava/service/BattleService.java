package com.example.datasourcinjava.service;


import com.example.datasourcinjava.entity.Battle;
import com.example.datasourcinjava.repositry.BattleRepository;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class BattleService {
    public final BattleRepository battleRepository;
    public BattleService(BattleRepository battleRepository){
        this.battleRepository=battleRepository;
    }


    public List<Battle> getAll(){
        return battleRepository.findAllBattle();
    }
}
