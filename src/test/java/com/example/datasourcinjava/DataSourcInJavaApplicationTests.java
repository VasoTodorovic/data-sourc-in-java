package com.example.datasourcinjava;

import com.example.datasourcinjava.entity.Battle;
import com.example.datasourcinjava.repositry.BattleRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

@SpringBootTest
@AutoConfigureMockMvc
class DataSourcInJavaApplicationTests {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private BattleRepository battleRepository;

	@Test
	void createBattleTest() throws Exception {
		ObjectMapper mapper = new ObjectMapper();
		Battle p = new Battle();
		p.setId(1);
		p.setName("Battle for Grozny");
		p.setDate_battle("1993");
		p.setParticipant(2);

		mockMvc.perform(post("/battles")
				.contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsString(p))
		).andExpect(status().isOk());
// Mockito.any(Battle.class))      vs     (p)
		verify(battleRepository).insertBattle(Mockito.any(Battle.class));
	}

	@Test
	void getBattle() throws Exception {
		ObjectMapper mapper = new ObjectMapper();

		Battle p = new Battle();
		p.setId(1);
		p.setName("Battle for Grozny");
		p.setDate_battle("1993");
		p.setParticipant(2);

		List<Battle> battles = List.of(p);

		when(battleRepository.findAllBattle()).thenReturn(battles);

		mockMvc.perform(get("/battles"))
				.andExpect(status().isOk())
				.andExpect(content().json(mapper.writeValueAsString(battles)));
	}

	@Test
	void  getAllBattle()throws  Exception {
		String s= mockMvc.perform(get("/battles")).andReturn().getResponse().getContentAsString();
		System.out.println(s);
	}

}
